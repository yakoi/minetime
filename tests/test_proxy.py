# -*- coding: utf-8 -*-

import unittest
from datetime import datetime

from mock import Mock, patch

from nose.tools import assert_equal, assert_true
from tabulate import tabulate

from .context import proxy


class ProxyTestSuite(unittest.TestCase):
    """Proxy test cases."""

    def setUp(self):
        self.uri = 'http://uri'
        self.key = 'abc'
        self.proxy = proxy.RedmineProxy(Mock(), self.uri, self.key)
        self.response = Mock()

    @patch('redmine.Redmine')
    def test_get_issues(self, mock_redmine):
        #patcher_issue = patch('redmine.Redmine.issue', return_value=self.response)
        mock_redmine.side_effect = ['a', 9, 3]

        #self.proxy.get_issues([])
        pass


if __name__ == '__main__':
    unittest.main()
