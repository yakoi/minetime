# -*- coding: utf-8 -*-

import os
import sys

import minetime
from minetime import cli, helpers
from minetime_lib import lib, proxy

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
