# -*- coding: utf-8 -*-


import unittest
from datetime import datetime

from mock import Mock, call, patch

from nose.tools import assert_equal, assert_true
from tabulate import tabulate

from .context import helpers, lib


class MinetimelibTestSuite(unittest.TestCase):
    """lib test cases."""

    def setUp(self):
        self.fixed_date_s = '2000-01-01'
        self.fixed_date = datetime.strptime(self.fixed_date_s, '%Y-%m-%d').date()
        self.timelog_1 = lib.Timelog(1001, 1.0, self.fixed_date, 9, '1st log on 1001')
        self.timelog_2 = lib.Timelog(1001, 2.0, self.fixed_date, 9, '2nd log on 1001')
        pass

    def test_vali_date(self):
        is_date = lib.vali_date('2013-13-13')
        self.assertFalse(is_date)

        is_date = lib.vali_date('2013-12-13')
        self.assertTrue(is_date)

    def test_merge_sieblings_x_x_merge(self):
        timelog_merged = lib.Timelog(1001, 3, self.fixed_date, 9,
                                               '1st log on 1001|2nd log on 1001')
        timelogs = [self.timelog_1, self.timelog_2]
        results = lib.merge_sieblings(timelogs, Mock())

        self.assertEqual(len(results), 1)
        self.assertEqual(results[0].issue_id, timelog_merged.issue_id)
        self.assertEqual(results[0].spent_on, timelog_merged.spent_on)

    def test_round_timelogs(self):
        timelog_113 = lib.Timelog(1001, 1.13, self.fixed_date, 9, 'comment')
        timelog_130 = lib.Timelog(1001, 1.3, self.fixed_date, 9, 'comment')
        original_timelogs = [timelog_113, timelog_130]

        timelog_rounded_125 = lib.Timelog(1001, 1.25, self.fixed_date, 9, 'comment')
        expected_rounded_timelogs = [timelog_rounded_125, timelog_rounded_125]

        rounded_timelogs = lib.round_timelogs(original_timelogs)

        self.assertEqual(len(rounded_timelogs), 2)
        self.assertEqual(rounded_timelogs[0].hours, timelog_rounded_125.hours)
        self.assertEqual(rounded_timelogs[1].hours, timelog_rounded_125.hours)

    def test_round_hours_113_125(self):
        rounded = lib.round_hours(1.13)
        self.assertEqual(rounded, 1.25)

    def test_round_hours_130_125(self):
        rounded = lib.round_hours(1.30)
        self.assertEqual(rounded, 1.25)

    def test_get_batch_timelogs_gtl(self):

        gtl_input = '''
            2016-12-02 07:00: hello
            2016-12-02 13:45: 12348 create crook cracker
            2016-12-03 05:00: arrived
            2016-12-03 05:45: 12345 bake breakfast
            2016-12-03 06:30: 12346 shower**
            2016-12-03 07:15: 12347:walk the spiders
            2016-12-03 08:00: minetime: 12348 fix the hamsters
            2016-12-03 09:00: 12349 stick the stickers
            2016-12-03 09:15: 12346 shower**
            2016-12-03 12:00: 12347:clean the dumpsters
            2016-12-03 12:45: 12346 shower
            2016-12-03 13:15: lunch ***
            2016-12-03 13:45: 12346 shower***
            2016-12-04 07:00: arrived
            2016-12-04 13:45: 12346 toast trumpet till toasted
        '''

        gtl_input = gtl_input.splitlines()

        tdate = datetime.strptime('2016-12-03', '%Y-%m-%d').date()
        config = {'user':{'activity_id':9}}

        timelogs = lib.get_batch_timelogs(Mock(), config, 'gtl', gtl_input, tdate, False)

        timelog_batch_1= lib.Timelog(12345, 0.75, tdate, 9,
                                          'bake breakfast')
        timelog_batch_2= lib.Timelog(12347, 3.50, tdate, 9,
                                          'clean the dumpsters|walk the spiders')
        timelog_batch_3= lib.Timelog(12348, 0.75, tdate, 9,
                                          'fix the hamsters')
        timelog_batch_4= lib.Timelog(12349, 1, tdate, 9,
                                          'stick the stickers')
        timelog_batch_5= lib.Timelog(12346, 0.75, tdate, 9,
                                               'shower')
        should_timelogs = [timelog_batch_1, timelog_batch_2,
                           timelog_batch_3, timelog_batch_4,
                           timelog_batch_5]

        self.assertEqual(set(timelogs), set(should_timelogs))

    def test_string_to_timelogs(self):
        input_string = """
  Id  Hours    Date           Activity     Comments
----  -------  ----------     -----------  -----------
1001       1   2000-01-01             9    1st log on 1001
1001       2   2000-01-01             9    2nd log on 1001
        """

        expected_timelogs = [self.timelog_1, self.timelog_2]
        timelogs = lib.string_to_timelogs(input_string)
        assert_equal(timelogs, expected_timelogs)

    def test_bad_string_to_timelogs(self):
        input_string = """
  Id  Hours    Date           Activity     Comments
----  -------  ----------     -----------  -----------
asdf
1001       2   2000-01-01             9    2nd log on 1001
        """

        expected_timelogs = [self.timelog_2]
        timelogs = lib.string_to_timelogs(input_string)
        assert_equal(timelogs, expected_timelogs)

    def test_humanize_date(self):
        anyday_string = '1900-01-01'
        anyday = datetime.strptime(anyday_string, '%Y-%m-%d').date()
        today = datetime.now().date()
        today_string = 'today'

        assert_equal(lib.humanize_date(anyday), anyday_string)
        assert_equal(lib.humanize_date(today), today_string)
