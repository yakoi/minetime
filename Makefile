init:
	pip install -r requirements.txt

build:
	python setup.py sdist
	echo "twine upload dist/minetime-*.tar.gz"

clean-pyc:
	find . -name '*.pyc' -exec rm --force {} +
	find . -name '*.pyo' -exec rm --force {} +
	find . -name '*~' -exec rm --force  {} +

clean-build:
	rm --force --recursive build/
	rm --force --recursive dist/
	rm --force --recursive *.egg-info
	python setup.py sdist
	echo "twine upload dist/minetime-*.tar.gz"

isort:
	sh -c "isort --skip-glob=.tox --recursive . "

lint:
	flake8 --exclude=.tox --ignore='E501'

test: clean-pyc
	nosetests tests

test-coverage: clean-pyc
	nosetests --with-coverage --cover-erase tests --cover-package=minetime --cover-package=minetime_lib

test-pdb: clean-pyc
	nosetests -vv -x -s --pdb tests
