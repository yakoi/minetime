# -*- coding: utf-8 -*-
import unittest
from datetime import datetime

from mock import Mock, call, patch

from nose.tools import assert_equal, assert_true

from .context import cli, lib, minetime


class CliTestSuite(unittest.TestCase):
    """Cli test cases."""

    def setUp(self):
        self.maxDiff = None
        self.fixed_date_s = '2000-01-01'
        self.fixed_date = datetime.strptime(self.fixed_date_s, '%Y-%m-%d').date()
        self.timelog_1 = lib.Timelog(1001, 1.0, self.fixed_date, 9, '1st log on 1001')
        self.timelog_2 = lib.Timelog(1001, 2.0, self.fixed_date, 9, '2nd log on 1001')

        self.first_issue = {'id': 331, 'subject': u'premier hello ڞ'}
        self.second_issue = {'id': 332, 'subject': u'deuxième hello ڞ'}
        self.issues = [[self.first_issue, ], [[self.second_issue, ], ]]

    @patch('pkg_resources.require')
    @patch('minetime.cli.cli_wizard_action')
    @patch('minetime_lib.lib.round_timelogs')
    @patch('minetime.cli.click.confirm')
    @patch('minetime.cli.click.edit')
    def test_cli_wizard(self, mock_edit, mock_confirm, mock_round, mock_action, mock_pkgr):

        mock_proxy = Mock()
        mock_config = {'activities': [], 'tracked_queries': [], 'user': {'activity_id': ''}}

        timelogs = [self.timelog_1, self.timelog_2]

        mock_action.side_effect = ['r', 'q']

        timelogs = cli.cli_wizard(mock_proxy, Mock(), mock_config, timelogs, Mock())

        mock_round.assert_called_once()
        assert_equal(timelogs, False)

    @patch('minetime.cli.click')
    def test_cli_wizard_choose_issue(self, mock_click):
        i1 = {'id': 1, 'subject': 'subject 1'}
        i2 = {'id': 2, 'subject': 'subject 1'}
        i3 = {'id': 3, 'subject': 'subject 3'}
        ilist = [i1, i2, i3]
        all_issues = (ilist, [])

        mock_click.prompt.side_effect = ['a', 9, 3]

        issue_id, issue_subject = cli.cli_wizard_choose_issue(all_issues)

        assert_equal(issue_id, 3)
        assert_equal(issue_subject, 'subject 3')
        assert_equal(mock_click.prompt.call_count, 3)

    @patch('os.makedirs')
    @patch('os.path.exists')
    @patch('minetime.cli.click.open_file')
    @patch('minetime.cli.click.prompt')
    @patch('minetime.cli.click.confirm')
    @patch('yaml.safe_dump')
    @patch('minetime.helpers.get_config')
    def test_init_config(self, mock_get_config, mock_safe_dump, mock_confirm,
                         mock_prompt, mock_open_file, mock_path_exists,
                         mock_makedirs):

        expected_url = 'http://3.3.3.3/'
        expected_key = '3' * 333

        mock_path_exists.return_value = False
        mock_get_config.return_value = True
        mock_confirm.return_value = True
        mock_confirm.return_value = True
        mock_prompt.side_effect = [expected_url, expected_key]
        config = cli.init_config()
        mock_makedirs.assert_called_once()
        mock_open_file.assert_called_once()
        mock_safe_dump.assert_called_once()

    @patch('minetime.cli.click.confirm')
    @patch('minetime.cli.click.edit')
    def test_cli_wizard_comments_message(self, mock_click_edit, mock_click_confirm):

        issue_id = 1001
        issue_subject = 'subject'
        hours = 3
        date = '2017-03-03'

        cli.cli_wizard_comments_message(issue_id, issue_subject, hours, date)

        mock_click_edit.assert_called_once()

    @patch('minetime.cli.click.confirm')
    @patch('minetime.cli.click.edit')
    def test_cli_wizard_comments_message_unconfirmed(self, mock_click_edit, mock_click_confirm):

        mock_click_confirm.return_value = False

        issue_id = 1001
        issue_subject = 'subject'
        hours = 3
        date = '2017-03-03'

        message = cli.cli_wizard_comments_message(issue_id, issue_subject, hours, date)

        mock_click_edit.assert_not_called()
        assert_equal(message, '')

    @patch('minetime.cli.click.edit')
    def test_cli_wizard_edit_timelogs(self, mock_click_edit):
        timelogs = [self.timelog_1, self.timelog_2]

        mock_click_edit.return_value = ''
        etimelogs = cli.cli_wizard_edit_timelogs(timelogs)
        assert_equal(etimelogs, timelogs)

        t1s = '1001  1.0  {} 9 1st log on 1001'.format(self.fixed_date)
        t2s = '1001  2.0  {} 9 2nd log on 1001'.format(self.fixed_date)

        mock_click_edit.return_value = '{}\n{}'.format(t1s, t2s)
        etimelogs = cli.cli_wizard_edit_timelogs(timelogs)
        assert_equal(etimelogs, timelogs)

    @patch('minetime.cli.click.prompt')
    def test_cli_wizard_action(self, mock_click_prompt):
        mock_click_prompt.return_value = 'p'
        r = cli.cli_wizard_action()
        assert_equal(r, 'p')

    @patch('minetime.cli.click.prompt')
    @patch('minetime.cli.cli_wizard_comments_message')
    @patch('minetime.cli.cli_wizard_choose_issue')
    def test_cli_wizard_new_timelog(self, mock_choose_issue, mock_comment,
                                    mock_click_prompt):

        mock_choose_issue.return_value = (1001, 'subject')
        mock_comment.return_value = '1st log on 1001'
        date = self.fixed_date
        mock_click_prompt.side_effect = [1, date]
        t = cli.cli_wizard_new_timelog(Mock(), 9, self.fixed_date)
        assert_equal(t, self.timelog_1)

    @patch('minetime_lib.lib.total_hours')
    def test_report_print_version_scenarios(self, mock_total_hours):
        mock_total_hours.return_value = 3
        s1 = Mock(story_points=3, time_entries=[], estimated_hours=9, status=Mock(id=2))
        s2 = Mock(story_points=5, time_entries=[], estimated_hours=15, status=Mock(id=1))
        s = [s1, s2]

        cli.report_print_version_scenarios(s)

        mock_total_hours.assert_has_calls([call([]), call([])])

    def test_cli_wizard_post_feedback_exit(self):

        attrs = {'post_timelogs.return_value': ([[1, 2, 3], [4, 5, 6]], [[7, 8, 9], [0, 1, 2]])}
        mock_proxy = Mock(**attrs)

        try:
            cli.cli_wizard_post_feedback_exit(mock_proxy, Mock())
        except SystemExit:
            assert(True)


    @patch('minetime.cli.click.echo')
    @patch('minetime.cli.click.prompt')
    def test_prompt_pid(self, mock_prompt, mock_echo):
        expected = 'expected'

        mock_prompt.return_value = 1
        config = {'tracked_projects': ['aaa',expected,'ccc']}

        pid = cli.prompt_pid(config)

        assert_equal(pid, expected)

    @patch('minetime.cli.click.echo')
    @patch('minetime.cli.click.prompt')
    def test_load_valid_project(self, mock_prompt, mock_echo):
        expected = 'git_helloworld'
        not_so = 'bad_bad_bad'
        mock1 = Mock()
        mock_prompt.side_effect = [not_so, expected]

        attrs = {'get_project.side_effect': [False, mock1]}
        mock_proxy = Mock(**attrs)

        project = cli.load_valid_project(not_so, mock_proxy)
        assert_equal(mock1, project)


if __name__ == '__main__':
    unittest.main()
